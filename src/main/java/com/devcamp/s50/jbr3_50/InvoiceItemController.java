package com.devcamp.s50.jbr3_50;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InvoiceItemController {
    @GetMapping("/invoices")
    @CrossOrigin
    public ArrayList<InVoiceItem> inVoiceItems(){
       InVoiceItem HoaDon1 = new InVoiceItem("1", "BanTrai", 4, 12000);
       InVoiceItem HoaDon2 = new InVoiceItem("2", "Thuoc", 24, 8000);
       InVoiceItem HoaDon3 = new InVoiceItem("3", "Bia", 24, 14000);

       System.out.println("Hoa Don 1" + HoaDon1);
       System.out.println("Hoa Don 1" + HoaDon2);
       System.out.println("Hoa Don 1" + HoaDon3);

       System.out.println("Tong Tien Hoa Don 1" + HoaDon1.getTotal());
       System.out.println("Tong Tien Hoa Don 2" + HoaDon2.getTotal());
       System.out.println("Tong Tien Hoa Don 3" + HoaDon3.getTotal());

       ArrayList<InVoiceItem> DanhSachHoaDon = new ArrayList<>();
       DanhSachHoaDon.add(HoaDon1);
       DanhSachHoaDon.add(HoaDon2);
       DanhSachHoaDon.add(HoaDon3);

       return DanhSachHoaDon ;

    }



  
}
